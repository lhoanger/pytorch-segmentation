import argparse
import yaml
import numpy as np
from pathlib import Path
from PIL import Image
from tqdm import tqdm
from torch.utils.data import Dataset

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import torch
from torch.utils.data import DataLoader
from glob import glob
from PIL import Image
import os, cv2

from models.net import EncoderDecoderNet, SPPNet
from utils.preprocess import minmax_normalize

IMAGE_EXT = '.png'
IMAGE_PATTERN = '_img' + IMAGE_EXT


class OmnivorData(Dataset):
  def __init__(self, dir, max_size = 512):
    self.image_paths = glob(dir + '/*' + IMAGE_PATTERN)
    self.images = []
    for i in range(len(self.image_paths)):
        img = Image.open(self.image_paths[i])
        width, height = img.size
        resize_ratio = 1.0 * max_size / max(width, height)
        target_size = (int(resize_ratio * width), int(resize_ratio * height))
        resized_image = np.array(img.convert('RGB').resize(target_size, Image.ANTIALIAS))
        resized_image = minmax_normalize(resized_image, norm_range=(-1, 1))
        resized_image = resized_image.transpose(2,0,1)
        self.images.append(resized_image)

    self.max_size = max_size
    
  def __len__(self):
    return len(self.image_paths)

  def __getitem__(self,idx):
    return torch.FloatTensor(self.images[idx]), os.path.basename(self.image_paths[idx])


def predict(batched, tta_flag=False):
    images, names = batched
    images_np = images.numpy()

    images = images.to(device)
    if tta_flag:
        preds = model.tta(images, scales=scales, net_type=net_type)
    else:
        preds = model.pred_resize(images, images.shape[2:], net_type=net_type)
    preds = preds.argmax(dim=1)
    preds_np = preds.detach().cpu().numpy().astype(np.uint8)
    return images_np, preds_np, names


parser = argparse.ArgumentParser()
parser.add_argument('--config_path', type=str, default='../config/omnivor_deeplabv3p.yaml')
parser.add_argument('--dir', type=str, default='/home/lhoang/data/test/big')
parser.add_argument('--outdir', type=str, default='/home/lhoang/data/outputconverteddeeplab')
parser.add_argument('--tta', action='store_true')
parser.add_argument('--vis', action='store_true')
args = parser.parse_args()
config_path = Path(args.config_path)
tta_flag = args.tta
vis_flag = args.vis

config = yaml.load(open(config_path))
net_config = config['Net']
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

modelname = config_path.stem
model_path = Path('../model') / modelname / 'model.pth'

net_type = 'deeplab'
model = SPPNet(**net_config)
model.to(device)
model.update_bn_eps()

param = torch.load(model_path)
model.load_state_dict(param)
del param

model.eval()

scales = [1]
valid_dataset = OmnivorData(dir=args.dir)
valid_loader = DataLoader(valid_dataset, batch_size=1, shuffle=False)


output_dir = Path('../output/omnivor_val') / (str(modelname) + '_tta' if tta_flag else modelname)
output_dir.mkdir(parents=True)

with torch.no_grad():
    for batched in tqdm(valid_loader):
        _, preds_np, names = predict(batched)
        mask = preds_np.squeeze().astype(np.uint8)
        fname, fext = os.path.splitext(os.path.basename(names[0]).replace(IMAGE_PATTERN, IMAGE_EXT))
        out_name = "b'{}'{}".format(fname, fext) # same name as tf code
        cv2.imwrite(os.path.join(args.outdir, out_name), mask)


            
