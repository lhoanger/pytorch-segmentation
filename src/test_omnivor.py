import argparse, os, sys, cv2
from PIL import Image
import numpy as np
from glob import glob
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf
import torch
from models.net import OmnivorNet


INPUT_SIZE = 513
MEAN_PIXEL_VALUE = 127.5


def load_tf_sess(frozen_path):
  graph_def = None
  graph = None
  with tf.gfile.GFile(frozen_path, "rb") as f:
    graph_def = tf.GraphDef()
    graph_def.ParseFromString(f.read())
  assert graph_def is not None, 'Graph definition not loaded properly'
  with tf.Graph().as_default() as graph:  # type: tf.Graph
    tf.import_graph_def(graph_def, name='')
  assert graph is not None, 'Graph not loaded properly'
  return tf.Session(graph=graph)


def load_pt_ckpt(ckpt_path):
  model = OmnivorNet(max_size = INPUT_SIZE)
  model.to(device)
  model.update_bn_eps()
  param = torch.load(ckpt_path)
  model.load_state_dict(param)
  del param
  model.eval()
  return model


def load_resized_image(path):
  image = Image.open(path)
  width, height = image.size
  resize_ratio = 1.0 * INPUT_SIZE / max(width, height)
  target_size = (int(resize_ratio * width), int(resize_ratio * height))
  resized_image = image.convert('RGB').resize(target_size, Image.ANTIALIAS)
  return np.asarray(resized_image)


def tf_predict(sess, image):
  tf_pred = sess.run('SemanticPredictions:0', feed_dict={'ImageTensor:0': [image]})
  return tf_pred[0]


def pt_predict(model, image):
  h, w = image.shape[:2]
  h_pad = max(0, INPUT_SIZE - h)
  w_pad = max(0, INPUT_SIZE - w)
  padded_image = np.pad(image.astype(np.float), ((0, h_pad), (0, w_pad), (0, 0)), 'constant',
    constant_values = MEAN_PIXEL_VALUE)
  image_tensor = torch.FloatTensor(padded_image).unsqueeze(0).to(device)
  with torch.no_grad():
    pt_pred = model(image_tensor)
    pt_mask = pt_pred.detach().cpu().numpy().squeeze().astype(np.uint8)
    return pt_mask[:h, :w]


def test_single_image(image_path, tf_sess, pt_model, out_dir = None):
  print(image_path)
  image = load_resized_image(image_path)
  tf_mask = tf_predict(tf_sess, image)
  pt_mask = pt_predict(pt_model, image)
  assert np.array_equal(tf_mask.shape, pt_mask.shape), 'Mask dimension mismatch, tensorflow: {}, pytorch: {}'.format(
    tf_mask.shape, pt_mask.shape)

  diff = tf_mask != pt_mask
  num_diff = np.sum(diff)
  iou = np.sum(np.logical_and(tf_mask == 15, pt_mask == 15)) / np.sum(np.logical_or(tf_mask == 15, pt_mask == 15))
  print('num_diff = {} ({:.4f}%), iou = {:.4f}%'.format(num_diff, num_diff * 100 / tf_mask.size, iou * 100))

  if out_dir is not None:
    mask = pt_mask.astype(np.uint8)
    fname, fext = os.path.splitext(os.path.basename(image_path))
    if fname.endswith('_img'):
      fname = fname[:-4]
    out_name = "b'{}'{}".format(fname, fext)
    cv2.imwrite(os.path.join(out_dir, out_name), mask)


def save_torch_script(pt_ckpt, out_file):
  model = OmnivorNet(max_size = INPUT_SIZE)
  model.to(device)
  model.update_bn_eps()
  param = torch.load(pt_ckpt)
  model.load_state_dict(param)
  del param
  module = torch.jit.trace(model.eval(), torch.rand(1, INPUT_SIZE, INPUT_SIZE, 3).to(device))
  torch.jit.save(module, out_file)
  print('Saved torch.jit model to ' + out_file)


def main(arguments):
  parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawDescriptionHelpFormatter)
  parser.add_argument('--tf_ckpt', type=str, help='The file name of the tensorflow checkpoint.')
  parser.add_argument('--tf_frozen', type=str, help='The file name of the tensorflow frozen graph.')
  parser.add_argument('--pt_ckpt', type=str, help='The file name of the pytorch checkpoint.')
  parser.add_argument('--image', type=str, help='Path to image file for testing.')
  parser.add_argument('--image_pattern', type=str, help='Pattern to search for images for testing.')
  parser.add_argument('--out_dir', type=str, default='/home/lhoang/data/outputconverteddeeplab')
  parser.add_argument('--save_torchscript', type=str, default='')
  args = parser.parse_args()
  print(args)

  global device
  device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

  if len(args.save_torchscript) > 0:
    save_torch_script(args.pt_ckpt, args.save_torchscript)
    return

  assert os.path.exists(args.tf_frozen), 'File not found: {}'.format(args.tf_frozen)
  assert os.path.exists(args.pt_ckpt), 'File not found: {}'.format(args.pt_ckpt)

  tf_sess = load_tf_sess(args.tf_frozen)
  pt_model = load_pt_ckpt(args.pt_ckpt)

  if args.image_pattern and len(args.image_pattern) > 0:
    images = glob(args.image_pattern)
    for path in images:
      test_single_image(path, tf_sess, pt_model, args.out_dir)
  else:
    assert os.path.exists(args.image), 'File not found: {}'.format(args.image)
    test_single_image(args.image, tf_sess, pt_model, args.out_dir)


if __name__ == '__main__':
  sys.exit(main(sys.argv[1:]))
